symfony 2 console command with gmail api
===
сode style PEAR standart

### Description

Symfony 2 console command for auto-receiving gmail attaches.
Authorization by keys, and ![not-alert][not-alert] with OAuth2
Available functions:

* list unread messages by label (INBOX)
* fetch email attaches to server
* mark messages as read
* manage push notifications [https://developers.google.com/gmail/api/guides/push#getting_gmail_mailbox_updates][gmail-push-url]


### composer requirements
```js
"symfony/console" : "*",
"google/apiclient": "1.0.*@beta", or later
```


#  Console should contain methods:

### create watch (push notifications)
Docs [https://developers.google.com/gmail/api/guides/push#getting_gmail_mailbox_updates][gmail-push-url]

```sh
bin/cli gmail -w
```

### list by label INBOX
list https://mail.google.com/mail/u/0/#inbox
```sh
bin/cli gmail -l inbox
```

### upload attaches by label
fetch new messages https://mail.google.com/mail/u/0/#inbox
save attaches to dirname(dirname(__file__) . '/upload/mail'
move to https://mail.google.com/mail/u/0/#read

```sh
bin/cli gmail -f inbox
```

[not-alert]: https://img.shields.io/badge/!!!-NOT-red.svg
[gmail-push-url]: https://developers.google.com/gmail/api/guides/push#getting_gmail_mailbox_updates
